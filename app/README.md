HelpExchange
=============

HelpExchange is a multi-language, collaborative q&a platform, specialized in serving the needs of refugees and other people
ending up in foreign countries.
