'use strict';

/** - Seperation of concerns - 
 * Don't move translation into a service or controller  
 * don't mix logic with presentational information
 * has to be moved into a seperate json file at some point
 **/

var translationsEN = {
    index : {
        profile: 'Profile',
        myquestions: 'My questions',
        settings: 'Settings',
        logout: 'Sign out'
    },
    home : { 
        home: {
            search: 'Search ...',
            close_by: 'Close by',
            near: 'near',
            translated_automatically: 'translated automatically',
            asked_by: 'asked by',
            trending: 'Trending',
            topics: 'Topics',
            general: 'General',
            overnight_stay: 'Overnight stay',
            orientation: 'Orientation',
            legal_information: 'Legal information',
            travelling: 'Travelling',
            food: 'Food',
            aid_in_kind: 'Aid in kind'
        },
        newquestion: {
            question_placeholder: 'What do you want to know ?',
            tags_placeholder: 'Tags (delimited by ",")',
            location_based_question: 'Location based question',
            location_based_question_description: 'Only answers which match your location are considered',
            private_question: 'Private question',
            private_question_description: 'This question will be sent to our staff',
            cancel: 'Cancel',
            submit_question: 'Submit question'
        }
    },
    login : {
        login: {
            title: 'Sign in to helpExchange',
            username_placeholder: 'Email or username',
            password_placeholder: 'Password',
            sign_in: 'Sign in'
        }
    },
    question: {
        newanswer: {
            answer_placeholder: 'Your answer ...',
            cancel: 'Cancel',
            submit: 'Submit'
        },
        question: {
            asked_by: 'asked by',
            answered_by: 'answered by',
            translated_automatically: 'translated automatically'
        }  
    }
};

var translationsDE = {
    index: {
        profile: 'Profil',
        myquestions: 'Meine Fragen',
        settings: 'Einstellungen',
        logout: 'Abmelden'
    },
    home : {
        home: { 
            search: 'Suche ...',
            close_by: 'Nahe',
            near: 'nahe',
            translated_automatically: 'automatisch übersetzt',
            asked_by: 'gestellt von',
            trending: 'Beliebt',
            topics: 'Themen',
            general: 'Allgemeines',
            overnight_stay: 'Nächtigung',
            orientation: 'Orientierung',
            legal_information: 'Rechtliches',
            travelling: 'Reiseinformation',
            food: 'Verpflegung',
            aid_in_kind: 'Sachhilfe'
        },
        newquestion: {
            question_placeholder: 'Neue Frage ...',
            tags_placeholder: 'Tags (durch "," getrennt)',
            location_based_question: 'Ortsbezogene Frage',
            location_based_question_description: 'Nur Antworten, die für meinen aktuellen Standort passen',
            private_question: 'Private Frage',
            private_question_description: 'Die Frage wird zunächst an eine/n Mitarbeiter/in gesendet',
            cancel: 'Abbrechen',
            submit_question: 'Frage stellen'
        }
    },
    login : {
        login: {
            title: 'Anmeldung',
            username_placeholder: 'Email / Benutzername',
            password_placeholder: 'Passwort',
            sign_in: 'Anmelden'
        }
    },
    question: {
        newanswer: {
            answer_placeholder: 'Neue Antwort ...',
            cancel: 'Abbrechen',
            submit: 'Antworten'
        },
        question: {
            asked_by: 'gestellt von',
            answered_by: 'beantworted von',
            translated_automatically: 'automatisch übersetzt'
        }  
    }
};

var helpExchangeApp = angular.module('helpExchangeApp', [
    'ngRoute',
    'ngMaterial',
    'firebase',
    'homeModule',
    'loginModule',
    'questionModule',
    'services',
    'pascalprecht.translate'
]);

helpExchangeApp.config(['$routeProvider', '$translateProvider',
    function ($routeProvider, $translateProvider) {
        $routeProvider.when('/', {
            templateUrl: 'home/home.html',
            controller: 'HomeController'
        }).when('/question/:id', {
            templateUrl: 'question/question.html',
            controller: 'QuestionController'
        }).when('/question/', {
            templateUrl: 'home/newQuestion.html',
            controller: 'HomeController'
        }).when('/login', {
            templateUrl: 'login/login.html',
            controller: 'LoginController'
        }).otherwise({
            redirectTo: '/login'
        });

        $translateProvider.translations('en', translationsEN);
        $translateProvider.translations('de', translationsDE);
        $translateProvider.preferredLanguage('en');
        $translateProvider.fallbackLanguage('en');

    }
]);

class Question {
    constructor(text, language, tags, isGeoSensitive, isPrivate, geoPosition, askedBy, ct) {
        this.lingualContent = {};
        this.lingualContent[language] = {
            'text': text,
            'isTranslated': false
        };
        this.tags = tags;
        this.isGeoSensitive = isGeoSensitive;
        this.isPrivate = isPrivate;
        this.geoPosition = geoPosition;
        this.askedBy = askedBy;
        this.views = 0;
        this.stars = 0;
        this.created = ct;
        this.modified = ct;
        this.answers = [];
    }
}

class Answer {
    constructor(text, language, answeredBy, ct) {
        this.lingualContent = {};
        this.lingualContent[language] = {
            'text': text,
            'isTranslated': false
        };
        this.answeredBy = answeredBy;
        this.upvotes = ['test'];
        this.downvotes = ['test'];
        this.created = ct;
        this.modified = ct;
    }
}

class GeoPosition {
    constructor(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
