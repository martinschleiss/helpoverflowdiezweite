'use strict';

var homeModule = angular.module('homeModule', [
    'ngMaterial',
    'firebase',
    'services'
]);

/**
 * Controller for the new question dialog
 */
function NewQuestionController($scope, $http, $firebaseArray, $mdDialog, $mdMedia, users, questions, geo, translate) {

    $scope.user = users.getCurrentUser();
    $scope.questions = questions.getAll();
    var getQuestionDefaults = function () {
        return {
            'isPrivate': false,
            'isGeoSensitive': false,
            'tags': '',
            'text': '',
            'geoPosition': {
                'latitude': 0,
                'longitude': 0
            }
        };
    };
    $scope.question = getQuestionDefaults();

    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    
    $scope.querying_location = false;
    $scope.location_error = false;
    
    var success = function (pos) {
        $scope.question.geoPosition.latitude = pos.coords.latitude;
        $scope.question.geoPosition.longitude = pos.coords.longitude;
        $scope.querying_location = false;
    };

    var error = function (err) {
        console.warn('Could not query location(' + err.code + '): ' + err.message);
        $scope.querying_location = false;
        $scope.location_error = true;
        $scope.question.isGeoSensitive = false;
    };
    
    $scope.location_trigger = function() {
        $scope.querying_location = true;
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition(success, error, options);
    };

    $scope.ask = function () {
        var text = $scope.question.text;
        var language = users.getCurrentUser().lang;
        var tags = $scope.question.tags;
        var isGeoSensitive = $scope.question.isGeoSensitive;
        var isPrivate = $scope.question.isPrivate;
        var geoPosition = $scope.question.geoPosition;
        var askedBy = users.getCurrentUser();
        var ct = (new Date()).getTime();

        translate.translate(text, language, function (translations) {
            var newQuestion = new Question(text, language, tags, isGeoSensitive, isPrivate, geoPosition, askedBy, ct);
            newQuestion.lingualContent = translations;
            $mdDialog.hide();
            $scope.questions.$add(newQuestion);
        });

    };
}

homeModule.controller('HomeController', ['$scope', '$http', '$firebaseArray', '$mdDialog', '$mdMedia', 'users', 'questions', 'geo',
    function ($scope, $http, $firebaseArray, $mdDialog, $mdMedia, users, questions, geo) {

        $scope.user = function () {
            return users.getCurrentUser();
        };
        $scope.questions = questions.getAll();

        $scope.inGeoProximity = geo.inGeoProximity;
        $scope.distance = geo.distance;

        geo.getCurrentPosition(function(position) {
            $scope.lat = position.coords.latitude;
            $scope.lng = position.coords.longitude;

            var map_options = {
                center: new google.maps.LatLng($scope.lat, $scope.lng),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("map_canvas"), map_options);

            var marker = new google.maps.Marker({
                map: map,
                zoom: 14
            });
            marker.setPosition(new google.maps.LatLng($scope.lat, $scope.lng));
        });

        /**
         * Pop up modal dialog for a new question
         */
        $scope.showNewQuestion = function (ev) {

            var useFullScreen = ($mdMedia('md') || $mdMedia('sm') || $mdMedia('xs'));
            $mdDialog.show({
                controller: NewQuestionController,
                templateUrl: 'home/newQuestion.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            });
        };
    }
]);
