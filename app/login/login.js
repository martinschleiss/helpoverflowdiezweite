var loginModule = angular.module('loginModule', [
  'services'
]);

loginModule.controller('LoginController', ['$scope', '$http', '$firebaseArray', '$mdDialog', '$mdMedia', '$translate', 'users', 'questions', 'geo', '$location',
    function($scope, $http, $firebaseArray, $mdDialog, $mdMedia, $translate, users, questions, geo, $location) {

        $scope.username = '';
        $scope.password = '';

        $scope.login = function () {
            var user = null;
            var userss = users.getUsers();
            for (var i = 0; i< userss.length; ++i) {
                var u = userss[i];
                if (u.username.toLowerCase() == $scope.username.toLowerCase() && u.password == $scope.password) {
                    user = u;
                }
            }
            if (user != null) {
                users.setCurrentUser(user.id);
                $translate.use(user.lang);
                $location.path("/");
            } else {
                alert('Wrong credentials');
            }

        }

    }
]);
