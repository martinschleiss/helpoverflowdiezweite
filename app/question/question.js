var questionModule = angular.module('questionModule', [
  'firebase',
  'services'
]);

/**
 * Controller for the new answer dialog
 */
function NewAnswerController($scope, $http, $routeParams, $firebaseArray, $mdDialog, $mdMedia, users, questions, geo, translate) {

  const ref = new Firebase("http://helpathon.firebaseio.com/questions");

  $scope.answ = {
    text: ''
  };
  $scope.user = users.getCurrentUser();
  $scope.questions = questions.getAll();
  var questionId = $routeParams.id;
  $scope.questions.$loaded().then(function(){
    $scope.question = $scope.questions.$getRecord(questionId);
  });

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function() {
    var text = $scope.answ.text;
    var language = users.getCurrentUser().lang;
    var answeredBy = users.getCurrentUser();
    var ct = new Date().getTime();
    var newAnswer = new Answer(text, language, answeredBy, ct);

    translate.translate(text, language, function(translations) {
      newAnswer.lingualContent = translations;

      var answers = $scope.question.answers;
      if (answers === undefined || answers === null) {
        $scope.question.answers = [];
      }
      $scope.question.answers.push(newAnswer);
      $mdDialog.hide();
      $scope.questions.$save($scope.question);
    });
  };
}

questionModule.controller('QuestionController', ['$scope', '$http', '$routeParams', '$mdDialog', '$mdMedia', '$firebaseArray', 'users', 'questions',
  function($scope, $http, $routeParams, $mdDialog, $mdMedia, $firebaseArray, users, questions) {

    $scope.user = users.getCurrentUser();
    $scope.questions = questions.getAll();
    var questionId = $routeParams.id;
    $scope.questions.$loaded().then(function(){
      $scope.question = $scope.questions.$getRecord(questionId);
    });

    $scope.votes = function(answer) {
      return answer.downvotes.length - answer.upvotes.length;
    };

    $scope.voteup = function(answer) {

      var upvotes = answer.upvotes;
      if (upvotes === undefined || upvotes === null) {
        answer.upvotes = [];
      }

      var index1 = answer.downvotes.indexOf($scope.user.username);
      if (index1 != -1) {
        answer.downvotes.splice(index1, 1);
      }

      var index2 = answer.upvotes.indexOf($scope.user.username);
      if (index2 == -1) {
        answer.upvotes.push($scope.user.username);
      } else {
        answer.upvotes.splice(index2, 1);
      }
      $scope.questions.$save($scope.question);
    };

    $scope.votedown = function(answer) {
      var downvotes = answer.downvotes;
      if (downvotes === undefined || downvotes === null) {
        answer.downvotes = [];
      }

      var index1 = answer.upvotes.indexOf($scope.user.username);
      if (index1 != -1) {
        answer.upvotes.splice(index1, 1);
      }

      var index2 = answer.downvotes.indexOf($scope.user.username);
      if (index2 == -1) {
        answer.downvotes.push($scope.user.username);
      } else {
        answer.downvotes.splice(index2, 1);
      }
      $scope.questions.$save($scope.question);
    };

    /**
     * Pop up modal dialog for a new answer
     */
    $scope.showNewAnswer = function(ev) {

      const useFullScreen = ($mdMedia('md') || $mdMedia('sm') || $mdMedia('xs'));
      $mdDialog.show({
        controller: NewAnswerController,
        templateUrl: 'question/newAnswer.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: useFullScreen
      });
    };
  }
]);
