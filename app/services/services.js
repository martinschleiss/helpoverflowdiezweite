'use strict';

var servicesModule = angular.module('services', [
  'firebase'
]);

servicesModule.service('users', function() {

  var users = [{
    "id": 0,
    "lang": "en",
    "password": "password",
    "rewards": 27,
    "username": "Martin"
  }, {
    "id": 1,
    "lang": "de",
    "password": "password",
    "rewards": 43,
    "username": "Michael"
  }, {
    "id": 2,
    "lang": "de",
    "password": "password",
    "rewards": 31,
    "username": "Rafael"
  }];

  var currentUser = -1;

  return {
    getUsers: function() {
      return users;
    },
    getCurrentUser: function() {
      return users[currentUser];
    },
    setCurrentUser: function(value) {
      currentUser = value;
    }
  };
});

servicesModule.service('questions', function($firebaseArray) {
  this.getAll = function() {

    var ref = new Firebase("http://helpathon.firebaseio.com/questions");
    var questions = $firebaseArray(ref);
    return questions;
  };

  this.create = function(question) {
    var messageListRef = new Firebase('http://helpathon.firebaseio.com/questions');
    messageListRef.push(question);
  };

  this.create = function(lingualContents, tags, isGeoSensitive, isPrivate, geoPosition, askedBy, views, stars) {
    var messageListRef = new Firebase('http://helpathon.firebaseio.com/questions');
    messageListRef.push({
      'lingualContents': lingualContents,
      'tags': tags,
      'isGeoSensitive': isGeoSensitive,
      'isPrivate': isPrivate,
      'geoPosition': geoPosition,
      'askedBy': askedBy,
      'views': views,
      'stars': stars
    });
  };

});

servicesModule.service('geo', function() {

  var currentGeoLocation = {
    latitude: null,
    longitude: null
  };
  var getCurrentGeoLocation = function() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(setPosition);
      return currentGeoLocation;
    } else {
      currentGeoLocation = {
        latitude: null,
        longitude: null
      };
    }
  };

  var setPosition = function(position) {
    currentGeoLocation.latitude = position.coords.latitude;
    currentGeoLocation.longitude = position.coords.longitude;
  };

  Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
  };

  var distance = function(longitude, latitude) {
    var lat1 = currentGeoLocation.latitude;
    var lat2 = latitude;
    var lon1 = currentGeoLocation.longitude;
    var lon2 = longitude;
    if (lat1 == null || lon1 == null) {
      return 999999;    // location unknown, near things cannot be guessed => distance is big
    }
    var R = 6371000; // metres
    var p1 = Math.radians(lat1);
    var p2 = Math.radians(lat2);
    var deltaP = Math.radians(lat2 - lat1);
    var deltaA = Math.radians(lon2 - lon1);

    var a = Math.sin(deltaP / 2) * Math.sin(deltaP / 2) +
      Math.cos(p1) * Math.cos(p2) *
      Math.sin(deltaA / 2) * Math.sin(deltaA / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  };

  var fun = function(lon, lat, maxDistance) {
    if (distance(lon, lat) < maxDistance) {
      return true;
    } else {
      return false;
    }
  };

  getCurrentGeoLocation();

  return {
    inGeoProximity: function(lat, lon, maxDistance) {
      return fun(lon, lat, maxDistance);
    },
    getCurrentPosition: function(setPosition) {
      navigator.geolocation.getCurrentPosition(setPosition);
    },
    distance: function(question) {
      var lon = question.geoPosition.longitude;
      var lat = question.geoPosition.latitude;
      return distance(lon, lat);
    }
  };
});

servicesModule.service('translate', function($http) {

  var param = {
    key: 'AIzaSyAZu7MJLkUqV04owTdHhAi78_eCdujLkrk',
    q: "Hallo Welt",
    source: "de",
    target: "en"
  };

  var translateIntoFun = function(text, sourceLang, targetLang, callback) {
    // TODO implement Google translation correctly
    var url = 'https://www.googleapis.com/language/translate/v2?key=' + param.key + '&source=' + sourceLang + '&target=' + targetLang + '&q=' + encodeURIComponent(text);
    console.log(url);

    $http({
      method: 'GET',
      url: url
    }).success(function(response) {
      //alert(response);
      // this callback will be called asynchronously
      // when the response is available
      //console.log('Google translated: ' + JSON.stringify(response));
      var trans = response.data.translations[0].translatedText;
      console.log(trans);
      callback(trans);
    }).error(function(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.error('Error: ' + response);
      throw new TranslationException('An error appeared: ' + response);
    });
  };

  var translateFun = function(text, sourceLang, callback) {
    var lingualContents = {};
    lingualContents[sourceLang] = {
      language: sourceLang,
      text: text,
      isTranslated: false
    };
    ['de', 'en', 'fa', 'ar'].forEach(function(targetLang) {   // fa ... persian, ar ... arabic
      if (targetLang !== sourceLang) {
        translateIntoFun(text, sourceLang, targetLang, function(translatedText) {
          lingualContents[targetLang] = {
            language: targetLang,
            text: translatedText,
            isTranslated: true,
            editedBy: null
          };
          if (lingualContents.de != undefined
            && lingualContents.en != undefined
            && lingualContents.ar != undefined
            && lingualContents.fa != undefined) {

            callback(lingualContents);  // wait for all languages to be translated into, then callback
          }
        });
      }
    });
  };

  return {
    getTranslations: function(text, sourceLang, targetLang, callback) {
      return translateIntoFun(text, sourceLang, targetLang, callback);
    },
    translate : function(text, sourceLang, callback) {
      return translateFun(text, sourceLang, callback);
    }
  };
});
