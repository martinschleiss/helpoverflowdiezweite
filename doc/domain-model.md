Domain Model
============

@startuml

class Question {
  LingualContent[], // one entry for each language
  Tags[],
  IsGeoSensitive,
  IsPrivate,
  GeoPosition,
  AskedBy,
  Views,
  Stars,
}

class Answer {
  LingualContent[], // one entry for each language
  AnsweredBy,
  Upvotes,
  Downvotes
}

class LingualContent {
  Language,
  Text,
  IsTranslated,
  EditedBy, // only if translated
}

class GeoPosition {
  Latitude,
  Longitude
}

class User {
  FirstName,
  LastName,
  Email,
  FirstLanguage,
  SecondLanguage,
  StaredQuestions[],
  UpvotedAnswers[],
  DownvotedAnwers[],
  Role,
  Score
}

class UserRole {
  Anonymous,
  LoggedIn,
  Editor,
  Verified,
  Admin
}

Question "1" *-- "1" GeoPosition
Question "1" *-- "*" Answer
User "*" -- "*" UserRole
Question ... LingualContent
Answer ... LingualContent
Question . User
Answer . User

@enduml