# Roadmap

## Ziel für Iteration 2 (Äquvivalent zu Lab 2)
* Fragen stellen, Fragen beantworten
* Automatische Übersetzung (4 Sprachen: Deutsch, Englisch, Arabisch, Persisch)
* Aufzeichnen von Up- und Downvotes

## Weitere Aufzeichnungen

1. Fertigstellen der automatischen Übersetzung
  * Übersetzen in alle unterstützen Sprachen zum Zeitpunkt der Fragestellung / Beantwortung
  * Suche von Fragen in allen unterstützten Fragen
  * Bearbeiten von Übersetzungen durch berechtigte Nutzerinnen
2. Implementierung eines einfachen Rating-Systems
  * Priorität von Fragen anhand von Views und Stars
  * Priorität von Antworten anhand der Upvotes
  * Priorität der Nutzerinnen anhand der persönlichen Scores
3. Visuelle Überarbeitung des UI
4. Authentifizierung
  * Registrierung mit Name und Email
  * Externe Services wie Facebook, Google
5. Rollenmodell für Nutzerinnen
  1. Nicht eingeloggte Nutzerin
  2. Eingeloggte Nutzerin
  3. Nutzerin mit Bearbeitungsrechten (z.B. Übersetzung, Löschen von Posts; kann durch bestimmten Score erreicht werden)
  4. Verifizierte Nutzerin (z.B. Mitglied einer Hilfsorganisation)
  5. Administratorin (Anlegen von verifizierten Nutzerinnen)
6. Flag / Report von Posts
7. Möglichkeit von privaten Fragen (?)

## Distribution

* Aufstellen von Bannern an hoch frequentierten Orten
* Autonomes Sharing durch diverse Kanäle am Telefon (Facebook, Viber, etc.)
